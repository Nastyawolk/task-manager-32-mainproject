package ru.t1.volkova.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.api.repository.IUserOwnedRepository;
import ru.t1.volkova.tm.model.AbstractUserOwnedModel;

@SuppressWarnings("UnusedReturnValue")
public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M> {

    @Nullable
    M create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

}
