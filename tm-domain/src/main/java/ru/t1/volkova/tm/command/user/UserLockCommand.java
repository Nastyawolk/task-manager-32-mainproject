package ru.t1.volkova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.enumerated.Role;
import ru.t1.volkova.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractUserCommand {

    @NotNull
    private static final String DESCRIPTION = "Lock user";

    @NotNull
    private static final String NAME = "user-lock";

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN");
        @NotNull final String login = TerminalUtil.nextLine();
        getUserService().lockUserByLogin(login);
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
