package ru.t1.volkova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.util.TerminalUtil;

public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "Bind task to project.";

    @NotNull
    private static final String NAME = "task-bind-to-project";

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("[ENTER PROJECT ID]");
        @NotNull final String projectID = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK ID]");
        @NotNull final String taskID = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        projectTaskService().bindTaskToProject(userId, projectID, taskID);
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
