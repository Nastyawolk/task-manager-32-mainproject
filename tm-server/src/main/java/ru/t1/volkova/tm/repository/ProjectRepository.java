package ru.t1.volkova.tm.repository;

import ru.t1.volkova.tm.api.repository.IProjectRepository;
import ru.t1.volkova.tm.model.Project;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

}
