package ru.t1.volkova.tm.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class ServerAboutResponse extends AbstractResponse {

    private String email;

    private String name;

}
