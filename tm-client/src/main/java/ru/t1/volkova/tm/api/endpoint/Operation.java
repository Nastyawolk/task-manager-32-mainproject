package ru.t1.volkova.tm.api.endpoint;

import ru.t1.volkova.tm.dto.request.AbstractRequest;
import ru.t1.volkova.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation <RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(RQ request);

}
