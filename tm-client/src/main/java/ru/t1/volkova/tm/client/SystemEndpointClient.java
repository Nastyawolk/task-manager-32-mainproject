package ru.t1.volkova.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.api.endpoint.ISystemEndpoint;
import ru.t1.volkova.tm.dto.request.ServerAboutRequest;
import ru.t1.volkova.tm.dto.request.ServerVersionRequest;
import ru.t1.volkova.tm.dto.response.ServerAboutResponse;
import ru.t1.volkova.tm.dto.response.ServerVersionResponse;

public final class SystemEndpointClient extends AbstractEndpoint implements ISystemEndpoint {

    @Override
    @NotNull
    @SneakyThrows
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        return (ServerAboutResponse) call(request);
    }

    @Override
    @NotNull
    @SneakyThrows
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request) {
        return (ServerVersionResponse) call(request);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull
        final SystemEndpointClient client = new SystemEndpointClient();
        client.connect();
        @NotNull
        final ServerAboutResponse serverAboutResponse = client.getAbout(new ServerAboutRequest());
        System.out.println(serverAboutResponse.getEmail());
        System.out.println(serverAboutResponse.getName());

        @NotNull
        final ServerVersionResponse serverVersionResponse = client.getVersion(new ServerVersionRequest());
        System.out.println(serverVersionResponse.getVersion());

        client.disconnect();
    }

}
